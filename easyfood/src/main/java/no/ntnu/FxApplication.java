package no.ntnu;

import java.io.IOException;
import java.sql.SQLException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Screen;
import javafx.stage.Stage;
import no.ntnu.controllers.InventoryController;
import no.ntnu.controllers.ShoppingController;
import no.ntnu.entityclasses.ListHandler;
import no.ntnu.entityclasses.ShoppingList;

public class FxApplication extends Application {

    @Override
    public void start(Stage stage) throws IOException, SQLException {

        Parent root = loadFXML("PrimaryView");
        Scene scene = new Scene(root);

        Screen screen = Screen.getPrimary();

        double screenWidth = screen.getBounds().getWidth();
        double screenHeight = screen.getBounds().getHeight();

        stage.setHeight(screenHeight);
        stage.setWidth(screenWidth);
        stage.setScene(scene);
        stage.setTitle("Fxml applicaion WIP");
        stage.show();

    }

    public void startFX(String[] args) {
        launch(args);
    }

    public static Parent loadFXML(String fxmlFileName) throws IOException {
        return FXMLLoader.load(FxApplication.class.getResource("/" + fxmlFileName + ".fxml"));
    }
}
