package no.ntnu;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        FxApplication app = new FxApplication();
        app.startFX(args);
    }
}