package no.ntnu;

import java.util.ArrayList;

import no.ntnu.entityclasses.Recipe;
import no.ntnu.servermanagement.Command;
import no.ntnu.servermanagement.ServerCommunication;

/**
 * Class for translating data between the server and the client
 * This class contains methods to convert the Command datatype that is recieved from @ServerCommunication
 * to the data types used in the client.
 * 
 * @since 0.0.2
 * @version 0.0.3
 * @author Joachim Duong
 */
public class ServerDataTranslator {

    /**
     * A method to get the data from the server and convert it to an ArrayList with data in the Inventory
     * @return ArrayList<String[]> with the data from the Inventory
     * 
     * @since 0.0.2
     */
    public ArrayList<String[]> getInventoryData() {
        Command serverResponse = ServerCommunication.getInstance().sendSpecifiedRequest(new String[] {}, (short) 13);
        if(serverResponse.commandID == 16){
            return serverResponse.databaseResponse;
        }
        else {
            return new ArrayList<>();
        }
    }

    /**
     * Inputs the inventory data to the server with an expiry date.
     * 
     * @since 0.0.2
     */
    public void inputInventoryData(String ingredient, String date, String stock) {
        ServerCommunication.getInstance().sendSpecifiedRequest(new String[] {ingredient, date, stock}, (short) 19);
    }

    /**
     * Inputs the inventory data to the server without an expiry date.
     * 
     * @since 0.0.2
     */
    public void inputInventoryData(String ingredient, String stock) {
        ServerCommunication.getInstance().sendSpecifiedRequest(new String[] {ingredient, stock}, (short) 19);
    }

    /**
     * Gets the ingredients from the server 
     * @return ArrayList<String> with the ingredients in the inventory
     * 
     * @since 0.0.2
     */
    public ArrayList<String> getIngredientsInventory() {
        ArrayList<String[]> data = getInventoryData();
        ArrayList<String> ingredients = new ArrayList<>();
        for(int n=0; n<data.size(); n++){
            ingredients.add(data.get(n)[0]);
        }
        return ingredients;
    }

    /**
     * Gets the dates from the server
     * @return ArrayList<String> with the dates in the inventory
     * 
     * @since 0.0.2
     */
    public ArrayList<String> getDatesInventory() {
        ArrayList<String[]> data = getInventoryData();
        ArrayList<String> dates = new ArrayList<>();
        for(int n=0; n<data.size(); n++){
            dates.add(data.get(n)[1]);
        }
        return dates;
    }

    /**
     * Gets the stock from the server
     * @return ArrayList<String> with the stock in the inventory
     * 
     * @since 0.0.2
     */
    public ArrayList<String> getStockInventory() {
        ArrayList<String[]> data = getInventoryData();
        ArrayList<String> stock = new ArrayList<>();
        for(int n=0; n<data.size(); n++){
            stock.add(data.get(n)[2]);
        }
        return stock;
    }

    /**
     * Gets the shopping list from the server 
     * @return ArrayList<String[]> with the data from the Recipe
     * 
     * @since 0.0.2
     */
    public ArrayList<String[]> getShoppingListData() {
        Command serverResponse = ServerCommunication.getInstance().sendSpecifiedRequest(new String[] {}, (short) 21);
        if(serverResponse.commandID == 32){
            return serverResponse.databaseResponse;
        }
        else {
            return new ArrayList<>();
        }
    }

    /**
     * Gets the ingredients from the server
     * 
     * @returns ArrayList<String> with the ingredients in the shopping list
     * 
     * @since 0.0.2
     */
    public ArrayList<String> getIngredientsShoppingList() {
        ArrayList<String[]> data = getShoppingListData();
        ArrayList<String> ingredients = new ArrayList<>();
        for(int n=0; n<data.size(); n++){
            ingredients.add(data.get(n)[0]);
        }
        return ingredients;
    }

    /**
     * Gets the shopping list data along with the quantity of each item
     * 
     * @return an arraylist with the quantity of each item in the shopping list
     * 
     * @since 0.0.2
     */
    public ArrayList<String> getAmountShoppingList() {
            ArrayList<String[]> data = getShoppingListData();
            ArrayList<String> amount = new ArrayList<>();
            for(int n=0; n<data.size(); n++){
                amount.add(data.get(n)[1]);
            }
            return amount;
    }

    /**
     * Gets the shopping list data along with the cost of each item
     * @return an arraylist with the cost of each item in the shopping list
     * 
     * @since 0.0.2
     */
    public ArrayList<String> getCostShoppingList() {
        ArrayList<String[]> data = getShoppingListData();
        ArrayList<String> cost = new ArrayList<>();
        for(int n=0; n<data.size(); n++){
            cost.add(data.get(n)[2]);
        }
        return cost;
    }

    /**
     * Gets the shopping list data along with the corresponding ingredient 
     * @return an arraylist with the ingredient of each item in the shopping list
     * 
     * @since 0.0.2
     */
    public ArrayList<String> getForShopping() {
        ArrayList<String[]> data = getShoppingListData();
        ArrayList<String> forList = new ArrayList<>();
        for(int n=0; n<data.size(); n++){
            forList.add(data.get(n)[3]);
        }
        return forList;
    }

    /**
     * Inputs the shopping list data to the server
     * 
     * @param ingredient the ingredient to send to the server
     * @param amount the amount of the ingredient to send to the server
     * @param cost the cost of the ingredient to send to the server
     * @param forString the corresponding ingredient to send to the server
     * 
     * @since 0.0.2
     */
    public void inputShoppingListData(String ingredient, String amount, String cost, String forString) {
        System.out.println(ServerCommunication.getInstance().sendSpecifiedRequest(new String[] {ingredient, amount, cost, forString}, (short) 20).commandID);
    }

    /**
     * Inputs the shopping list data to the server
     * 
     * @param ingredient the ingredient to send to the server
     * @param amount the amount of the ingredient to send to the server
     * @param forString the corresponding ingredient to send to the server
     * 
     * @since 0.0.2
     */
    public void inputShoppingListData(String ingredient, String amount, String forString) {
        System.out.println(ServerCommunication.getInstance().sendSpecifiedRequest(new String[] {ingredient, amount, forString}, (short) 20).commandID);
    }

    private ArrayList<String[]> getRecipeListData() {
        Command serverResponse = ServerCommunication.getInstance().sendSpecifiedRequest(new String[] {}, (short) 14);
        if(serverResponse.commandID == 18){
            return serverResponse.databaseResponse;
        }
        else {
            return new ArrayList<>();
        }
    }

    /**
     * Gets the recipe id from the name of a recipe
     * 
     * @param name the name of the recipe
     * @return the id of the recipe
     * 
     * @since 0.0.2
     */
    public int getRecipeIdFromName(String name) {
        ArrayList<String[]> data = getRecipeListData();
        int recipeId = -1;
        for (String[] datum : data) {
            if (datum[1].equals(name)) {
                recipeId = Integer.parseInt(datum[0]);
            }
        }
        return recipeId;
    }

    /**
     * Gets the recipe list from the server
     * 
     * @return an arraylist containing the all of the user's recipes
     * 
     * @since 0.0.2
     */
    public ArrayList<String> getRecipeList(){
        ArrayList<String[]> data = getRecipeListData();
        ArrayList<String> recipeIds = new ArrayList<>();
        for(int n=0; n<data.size(); n++){
            recipeIds.add(data.get(n)[0]);
        }
        return recipeIds;
    }

    /**
     * Gets the recipe name from the id of a recipe
     * 
     * @param id the id of the recipe
     * 
     * @since 0.0.2
     */
    public String getRecipeNameFromId(int id) {
        ArrayList<String[]> data = getRecipeListData();
        for(int n=0; n<data.size(); n++){
            if(Integer.parseInt(data.get(n)[0]) == id){
                return data.get(n)[1];
            }
        }
        return "";
    }

    /**
     * Gets the steps data from the server
     * 
     * @return an arraylist with the steps of the recipe
     * 
     * @since 0.0.2
     */
    public ArrayList<String[]> getStepsData(){
        Command serverResponse = ServerCommunication.getInstance().sendSpecifiedRequest(new String[] {}, (short) 15);
        if(serverResponse.commandID == 20){
            return serverResponse.databaseResponse;
        }
        else {
            return new ArrayList<>();
        }
    }

    /**
     * Gets the ingredients data from the server
     * 
     * @return an arraylist with the ingredients of the recipe
     * 
     * @since 0.0.2
     */
    public ArrayList<String[]> getIngredientsData(){
        Command serverResponse = ServerCommunication.getInstance().sendSpecifiedRequest(new String[] {}, (short) 12);
        if(serverResponse.commandID == 14){
            return serverResponse.databaseResponse;
        }
        else {
            return new ArrayList<>();
        }
    }

    /**
     * Create a new recipe and sends it to the server
     * 
     * @param recipeName the name of the recipe
     * 
     * @since 0.0.2
     */
    public void inputRecipeData(String recipeName) {
        ServerCommunication.getInstance().sendSpecifiedRequest(new String[] {recipeName}, (short) 16);
    }

    /**
     * Inputs the recipe data to the server
     * 
     * @param recipeName the name of the recipe
     * @param recipe_id the id of the recipe
     * @param amount the amount of the ingredient
     * 
     * @since 0.0.2
     */
    public void inputRecipeIngredient(String recipeName, String recipe_id, String amount) {
        ServerCommunication.getInstance().sendSpecifiedRequest(new String[] {recipeName, recipe_id, amount}, (short) 17);
    }

    /**
     * Inputs the recipe data to the server
     * 
     * @param recipe_id the id of the recipe
     * @param step the step of the recipe
     * 
     * @since 0.0.2
     */
    public void inputRecipeStep(String recipe_id, String step) {
        ServerCommunication.getInstance().sendSpecifiedRequest(new String[] {recipe_id, step}, (short) 18);
    }

    /**
     * Deletes the specified item from the shopping cart
     * @param itemId the id of the item to delete
     * 
     * @since 0.0.2
     */
    public void deleteFromShoppingCart(String itemId) {
        ServerCommunication.getInstance().sendSpecifiedRequest(new String[] {"Shopping_list", itemId}, (short) 23);
    }

    /**
     * Deletes the specified item from the inventory
     * @param itemId the id of the item to delete
     * 
     * @since 0.0.2
     */
    public void deleteFromInventory(String itemId) {
        System.out.println(ServerCommunication.getInstance().sendSpecifiedRequest(new String[] {"Inventories", itemId}, (short) 26).commandID);
    }

    /**
     * Deletes the specified item from the recipe
     * @param recipeId the id of the recipe to delete
     * 
     * @since 0.0.2
     */
    public void deleteRecipe(int recipeId) {
        ServerCommunication.getInstance().sendSpecifiedRequest(new String[] {"Recipes", Integer.toString(recipeId)}, (short) 22);
    }
}
