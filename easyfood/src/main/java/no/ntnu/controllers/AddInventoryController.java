package no.ntnu.controllers;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import no.ntnu.ServerDataTranslator;
import no.ntnu.entityclasses.ListHandler;
import no.ntnu.entityclasses.ProductManager;
import no.ntnu.servermanagement.ServerCommunication;

/**
 * The controller for the AddInventoryView.fxml file.
 * It is responsible for adding a product to the inventory list.
 * 
 * @since 0.0.2
 * @version 0.0.3
 * @author Joachim Duong
 */
public class AddInventoryController implements javafx.fxml.Initializable{
    private ProductManager inventoryList;
    @FXML
    private TextField ProductField;
    @FXML
    private TextField QuantityField;
    @FXML
    private TextField DateField;

    /**
     * Initializes the controller.
     * 
     * @since 0.0.2
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        inventoryList = ListHandler.getInstance().getInventory();
    }

    /**
     * Adds a product to the inventory list.
     * @param actionEvent the automatic event that is triggered when the button is clicked
     * 
     * @since 0.0.2
     */
    @FXML
    public void addProduct(javafx.event.ActionEvent actionEvent) {
        ServerDataTranslator serverDataTranslator = ServerCommunication.getInstance().getDataTranslator();
        String productName = ProductField.getText();
        int quantity = Integer.parseInt(QuantityField.getText());
        String dateString = DateField.getText();
        if(dateString.isEmpty()) {
            serverDataTranslator.inputInventoryData(productName, Integer.toString(quantity));
        }
        else {
            serverDataTranslator.inputInventoryData(productName, dateString,
                Integer.toString(quantity));
        }

        Stage stage = (Stage) ProductField.getScene().getWindow();
        stage.close();
        ListHandler.getInstance().getInventoryController().updateUI();
    }

}
