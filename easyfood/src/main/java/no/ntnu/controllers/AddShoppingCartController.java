package no.ntnu.controllers;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.scene.control.ListView;
import no.ntnu.entityclasses.ListHandler;
import no.ntnu.entityclasses.Recipe;

/**
 * Controller class for the AddShoppingCartView.fxml file.
 * It is responsible for adding items to the shopping cart.
 * 
 * @since 0.0.3
 * @version 0.0.3
 * @author Thomas Balsvik
 */
public class AddShoppingCartController implements javafx.fxml.Initializable {
    @FXML
    private ListView<String> inventoryList;
    @FXML
    private ListView<String> shoppingList;
    private Recipe currentRecipe;

    /**
     * Initializes the controller and updates it to the current shopping list. 
     * 
     * @since 0.0.2
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        currentRecipe = ListHandler.getInstance().getRecipeListController().getSelectedRecipe();
        updateUI();
    }

    private void updateUI() {
        inventoryList.getItems().setAll(ListHandler.getInstance().getInventoryController().getIngredients());
        shoppingList.getItems().setAll(ListHandler.getInstance().getShoppingController().getIngredients());
    }
}
