package no.ntnu.controllers;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import no.ntnu.ServerDataTranslator;
import no.ntnu.entityclasses.ListHandler;
import no.ntnu.entityclasses.Product;
import no.ntnu.entityclasses.ProductManager;
import no.ntnu.entityclasses.ShoppingList;
import no.ntnu.servermanagement.ServerCommunication;

/**
 * The controller for the AddShoppingView.fxml file.
 * It is responsible for adding products to the shopping list.
 * 
 * @since 0.0.2
 * @version 0.0.3
 * @author Joachim Duong
 */
public class AddShoppingController implements javafx.fxml.Initializable{
    private Button AddButton;
    private ProductManager shoppingList;
    @FXML
    private TextField ProductField;
    @FXML
    private TextField QuantityField;
    @FXML
    private TextField ForField;
    @FXML
    private TextField PriceField;

    /**
     * Initializes the controller. 
     *  
     * @since 0.0.2
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        shoppingList = ListHandler.getInstance().getShoppingList();
    }

    /**
     * Adds a product to the shopping list.
     * @param actionEvent the automatic event that is triggered when the button is clicked
     * 
     * @since 0.0.2
     */
    @FXML
    public void addProduct(javafx.event.ActionEvent actionEvent) {
        String productName = ProductField.getText();
        int quantity = Integer.parseInt(QuantityField.getText());
        String forString = ForField.getText();
        int price = Integer.parseInt(PriceField.getText());

        ServerDataTranslator serverDataTranslator = ServerCommunication.getInstance().getDataTranslator();
        if((price == 0)) {
            serverDataTranslator.inputShoppingListData(productName, Integer.toString(quantity), forString);
        }
        else {
            serverDataTranslator.inputShoppingListData(productName, Integer.toString(quantity), Integer.toString(price), forString);
        }
        Stage stage = (Stage) ProductField.getScene().getWindow();
        stage.close();
        ListHandler.getInstance().getShoppingController().updateUI();
    }
}
