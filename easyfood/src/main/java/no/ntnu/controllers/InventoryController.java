package no.ntnu.controllers;

import java.io.IOException;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import no.ntnu.FxApplication;
import no.ntnu.ServerDataTranslator;
import no.ntnu.entityclasses.Inventory;
import no.ntnu.entityclasses.ListHandler;
import no.ntnu.entityclasses.Product;
import no.ntnu.entityclasses.ProductManager;
import no.ntnu.entityclasses.ShoppingList;
import no.ntnu.servermanagement.ServerCommunication;

/**
 * The controller for the InventoryView.fxml file.
 * It is responsible for displaying the inventory list and handling the user's interaction with it.
 * 
 * @since 0.0.2
 * @version 0.0.3
 * @author Joachim Duong
 */
public class InventoryController implements javafx.fxml.Initializable{

    public Button GoRecipes;
    public Button GoShopping;
    private ProductManager inventoryList;
    @FXML
    private ListView<String> ingredientList;

    @FXML
    private ListView<String> stockList;
    @FXML
    private ListView<String> dateList;

    private int selectedIndex;

    /**
     * Initializes the controller and updates the inventory list.
     * 
     * @since 0.0.2
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        inventoryList = ListHandler.getInstance().getInventory();
        ListHandler.getInstance().setInventoryController(this);
        System.out.println("Inventory initialized");
        updateUI();
        synchList();
        setEventHandlers();
    }

    @FXML
    private void Add(ActionEvent event) throws IOException {
        Parent root = loadFXML("AddInventoryView");
        // Create a new stage
        Stage secondStage = new Stage();
        secondStage.setTitle("Add Window");
        secondStage.setScene(new Scene(root, 900, 580));
        secondStage.show();
    }
    
    @FXML
    private void remove(ActionEvent event) {
        removeProduct();
    }

    /**
     * Updates the list of ingredients, stock and expiration dates in the inventory list 
     * with the data from the server.
     * 
     * @since 0.0.2
     */
    public void updateUI() {
        updateFromServer();

        ingredientList.getItems().setAll(getIngredients());
        dateList.getItems().setAll(getDate());
        stockList.getItems().setAll(getStock());
    }

    /**
     * Returns a list of ingredients from the inventory list.
     * @return A list of ingredients from the inventory list.
     * 
     * @since 0.0.2
     */
    public List<String> getIngredients() {
        List<String> ingredients = new ArrayList<>();
        for (Product p : inventoryList.getProducts()) {
            ingredients.add(p.getName());
        }
        return ingredients;
    }

    private List<String> getStock() {
        List<String> stockList = new ArrayList<>();
        for (Product p : inventoryList.getProducts()) {
            stockList.add(Integer.toString(p.getQuantity()));
        }
        return stockList;
    }

    private List<String> getDate() {
        List<String> dateList = new ArrayList<>();
        for (Product p : inventoryList.getProducts()) {
            if (p.getExpirationDate() != null) {
                dateList.add(p.getExpirationDate());
            }
            else {
                dateList.add("");
            }
        }
        return dateList;
    }

    /**
     * Changes the scene to the recipe list view.
     * @param actionEvent the automatic event that is triggered when the button is clicked.
     * @throws IOException if the fxml file for the recipe list view is not found.
     * 
     * @since 0.0.2
     */
    public void goToRecipes(ActionEvent actionEvent) throws IOException {
        Scene scene =((Node) actionEvent.getSource()).getScene();
        scene.setRoot(FxApplication.loadFXML("RecipeListView"));
    }

    /**
     * Changes the scene to the shopping list view.
     * @param actionEvent the automatic event that is triggered when the button is clicked.
     * @throws IOException if the fxml file for the shopping list view is not found.
     * 
     * @since 0.0.2
     */
    public void goToShoppingList(ActionEvent actionEvent) throws IOException {
        Scene scene =((Node) actionEvent.getSource()).getScene();
        scene.setRoot(FxApplication.loadFXML("ShoppingListView"));
    }

    private void synchList() {
        ingredientList.getSelectionModel().selectedIndexProperty().addListener((observable, oldValue, newValue) -> {
            synchronizeSelection(newValue.intValue());
        });
        stockList.getSelectionModel().selectedIndexProperty().addListener((observable, oldValue, newValue) -> {
            synchronizeSelection(newValue.intValue());
        });
        dateList.getSelectionModel().selectedIndexProperty().addListener((observable, oldValue, newValue) -> {
            synchronizeSelection(newValue.intValue());
        });
    }
    private void synchronizeSelection(int selectedIndex) {
        if (selectedIndex >= 0) {
            ingredientList.getSelectionModel().select(selectedIndex);
            stockList.getSelectionModel().select(selectedIndex);
            dateList.getSelectionModel().select(selectedIndex);
        }
    }

    private void setEventHandlers() {
        ingredientList.setOnMouseClicked(event -> handleListViewClick(ingredientList, event));
        stockList.setOnMouseClicked(event -> handleListViewClick(stockList, event));
        dateList.setOnMouseClicked(event -> handleListViewClick(dateList, event));
    }

    private void handleListViewClick(ListView<String> listView, MouseEvent event) {
        selectedIndex = listView.getSelectionModel().getSelectedIndex();
        System.out.println("Selected index: " + selectedIndex);

    }

    private void removeProduct() {
        ServerCommunication.getInstance().getDataTranslator().deleteFromInventory(Integer.toString(inventoryList.getProducts().get(selectedIndex).getProductId()));
        selectedIndex--;
        updateUI();
    }

    /**
     * Sets the inventory list to the given inventory list.
     * @param inventoryList the inventory list to set.
     * 
     * @since 0.0.2
     */
    public void setInventoryList(ProductManager inventoryList) {
        if(inventoryList == null) {
            throw new IllegalArgumentException("Cant set inventory to null");
        }
        this.inventoryList = inventoryList;
    }

    /**
     * Used to load FXML files. 
     * @param fxmlFileName the filename without .fxml extension nor a beginning slash
     * @return A parent object that can be used to display a new scene
     * @throws IOException if the file is not found, meaning the fxmlFileName is incorrect.
     * 
     * @since 0.0.2
     */
    public Parent loadFXML(String fxmlFileName) throws IOException {
        return FXMLLoader.load(FxApplication.class.getResource("/" + fxmlFileName + ".fxml"));
    }

    private void updateFromServer(){
        inventoryList.clear();
        ServerDataTranslator translator = ServerCommunication.getInstance().getDataTranslator();
        int i = translator.getIngredientsInventory().size();
        ArrayList<String[]> data = translator.getInventoryData();
        for(int n = 0; n < i; n++){
            String currentName = data.get(n)[0];
            String currentDate = data.get(n)[1];
            int currentStock = Integer.parseInt(data.get(n)[2]);
            int currentId = Integer.parseInt(data.get(n)[3]);

            if(currentDate.equals("-1")){
                currentDate = null;
            }

            if(currentStock < 0){
                currentStock = 0;
            }

            Product p = new Product(currentName, currentDate, currentStock, currentId);
            inventoryList.addProduct(p);
        }
    }
}
