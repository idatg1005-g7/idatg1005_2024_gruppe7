package no.ntnu.controllers;

import java.lang.reflect.Array;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import no.ntnu.ServerDataTranslator;
import no.ntnu.entityclasses.*;
import no.ntnu.servermanagement.Command;
import no.ntnu.servermanagement.ServerCommunication;

/**
 * The controller for the RecipeAddView.fxml file.
 * It is responsible for adding a recipe to the recipe list.
 * 
 * @since 0.0.2
 * @version 0.0.3
 * @author Rolf Normann Flatøy
 */
public class RecipeAddController implements javafx.fxml.Initializable{

    @FXML
    private TextField ingredientInput;
    @FXML
    private TextField stepInput;
    private RecipeListController recipeListController;
    private Recipe currentRecipe;
    @FXML
    private ListView<String> stepList;
    @FXML
    private ListView<String> ingredientList;
    private int selectedIndexStep;
    private int selectedIndexIngredient;

    /**
     * Initializes the controller, while also setting up the ListView for
     * ingredients and steps of a specific recipe.
     * 
     * @since 0.0.2
     */
    @Override public void initialize(URL url, ResourceBundle rb) {
        recipeListController = ListHandler.getInstance().getRecipeListController();
        ListHandler.getInstance().setRecipeAddController(this);
        setCurrentRecipe(recipeListController.getSelectedRecipe());
        updateUI();
        setEventHandlers();
    }

    private void setEventHandlers() {
        ingredientList.setOnMouseClicked(event -> handleListViewClickIngredient(ingredientList, event));
        stepList.setOnMouseClicked(event -> handleListViewClickStep(stepList, event));
    }

    private void handleListViewClickIngredient(ListView<String> listView, MouseEvent event) {
        selectedIndexIngredient = listView.getSelectionModel().getSelectedIndex();
    }

    private void handleListViewClickStep(ListView<String> listView, MouseEvent event) {
        selectedIndexStep = listView.getSelectionModel().getSelectedIndex();
    }

    /**
     * Updates the UI of the recipe add window.
     * 
     * @since 0.0.2
     */ 
    public void updateUI() {
        stepList.getItems().setAll(currentRecipe.getStepsAsString());
        ingredientList.getItems().setAll(getIngredients());
    }

    /**
     * Sets the current recipe to the recipe that is selected in the recipe list.
     * @param recipe
     * 
     * @since 0.0.2
     */
    public void setCurrentRecipe(Recipe recipe) {
        currentRecipe = recipe;
    }

    private List<String> getIngredients() {
        List<String> ingredientsList = new ArrayList<>();
        for (Product p : currentRecipe.getProducts()) {
            ingredientsList.add(p.getName());
        }
        return ingredientsList;
    }

    /**
     * Adds a product to the current recipe and closes this window.
     * @param actionEvent the automatic action event that is passed when the button is clicked.
     * 
     * @since 0.0.2
     */
    public void addAndClose(ActionEvent actionEvent) {
        Stage stage = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
        stage.close();
        int currentRecipeId = currentRecipe.getRecipeId();
        addToServer();
        ListHandler.getInstance().getRecipeListController().updateUI();
        Recipe tempCurrentRecipe = ListHandler.getInstance().getRecipeManager().getRecipeFromId(currentRecipeId);
        ListHandler.getInstance().getRecipeListController().setSelectedRecipe(tempCurrentRecipe);
        if(ListHandler.getInstance().getRecipeController() != null) {
            ListHandler.getInstance().getRecipeController().updateUI();
        }
    }

    /**
     * Adds a product to the current recipe.
     * @param keyEvent the automatic key event that is passed when a key is pressed in a textfield.
     * 
     * @since 0.0.2
     */
    @FXML
    public void ingredientFieldPressed(KeyEvent keyEvent) {
        if (keyEvent.getCode().getName().equalsIgnoreCase("enter")) {
            addIngredient();
        }
    }

    /**
     * Adds a step to the current recipe.
     * @param keyEvent the automatic key event that is passed when a key is pressed in a textfield.
     * 
     * @since 0.0.2
     */
    @FXML
    public void stepFieldPressed(KeyEvent keyEvent) {
        if (keyEvent.getCode().getName().equalsIgnoreCase("enter")) {
            addStep();
        }

    }

    private void addIngredient(){
        String inputText = ingredientInput.getText();
        if (inputText != null && !inputText.isEmpty()) {
            currentRecipe.addProduct(new Product(inputText));
        }
        ingredientInput.clear();
        updateUI();
    }

    private void addStep(){
        String inputText = stepInput.getText();
        if (inputText != null && !inputText.isEmpty()) {
            currentRecipe.addStep(inputText, -1);
        }
        stepInput.clear();
        updateUI();
    }

    /**
     * Moves the currently selected ingredient up one step. This is a client side only event.
     * @param actionEvent the automatic action event that is passed when the button is clicked.
     * 
     * @since 0.0.2
     */
    public void moveIngredientUp(ActionEvent actionEvent) {
        if(selectedIndexIngredient > 0 && selectedIndexIngredient <= currentRecipe.getProducts().size()) {
            currentRecipe.moveProductUp(selectedIndexIngredient);
        }
        selectedIndexIngredient = -1;
        updateUI();
    }

    /**
     * Moves the currently selected ingredient down one step. This is a client side only event.
     * @param actionEvent the automatic action event that is passed when the button is clicked.
     * 
     * @since 0.0.2
     */
    public void moveIngredientDown(ActionEvent actionEvent) {
        if(selectedIndexIngredient >= 0 && selectedIndexIngredient < currentRecipe.getProducts().size() - 1) {
            currentRecipe.moveProductDown(selectedIndexIngredient);
        }
        selectedIndexIngredient = -1;
        updateUI();
    }

    /**
     * Moves the currently selected step up one step. This is a client side only event.
     * @param actionEvent the automatic action event that is passed when the button is clicked.
     * 
     * @since 0.0.2
     */
    public void moveStepUp(ActionEvent actionEvent) {
        if(selectedIndexStep > 0 && selectedIndexStep <= currentRecipe.getSteps().size()) {
            currentRecipe.moveStepUp(selectedIndexStep);
        }
        selectedIndexStep = -1;
        updateUI();
    }

    /**
     * Moves the currently selected step down one step. This is a client side only event.
     * @param actionEvent the automatic action event that is passed when the button is clicked.
     * 
     * @since 0.0.2
     */
    public void moveStepDown(ActionEvent actionEvent) {
        if(selectedIndexStep >= 0 && selectedIndexStep < currentRecipe.getSteps().size() - 1) {
            currentRecipe.moveStepDown(selectedIndexStep);
        }
        selectedIndexStep = -1;
        updateUI();
    }

    /**
     * Adds the current recipe with all the steps and ingredients to the server. 
     * 
     * @since 0.0.2
     */
    public void addToServer() {
        ArrayList<String[]> stepdata = ServerCommunication.getInstance().getDataTranslator().getStepsData();
        ArrayList<String[]> ingredientdata = ServerCommunication.getInstance().getDataTranslator().getIngredientsData();

        for(int i = 0; i < stepdata.size(); i++) {
            Iterator<Step> stepIterator = currentRecipe.getSteps().iterator();
            while(stepIterator.hasNext()) {
                Step step = stepIterator.next();
                if(step.getStepId() == Integer.parseInt(stepdata.get(i)[0])) {
                    stepIterator.remove();
                }
            }
        }

        for(int i = 0; i < ingredientdata.size(); i++) {
            Iterator<Product> productIterator = currentRecipe.getProducts().iterator();
            while(productIterator.hasNext()) {
                Product product = productIterator.next();
                if((product.getProductId() == Integer.parseInt(ingredientdata.get(i)[3]))) {
                    productIterator.remove();
                }
            }
        }

        ArrayList<Product> products = currentRecipe.getProducts();
        ArrayList<String> steps = currentRecipe.getStepsAsString();

        int recipeID = currentRecipe.getRecipeId();
        for(int i = 0; i < products.size(); i++) {
            Product p = products.get(i);
            System.out.println("Added" + p.getName());
            System.out.println(i);
            ServerCommunication.getInstance().getDataTranslator().inputRecipeIngredient(p.getName(), Integer.toString(recipeID), Integer.toString(p.getQuantity()));
        }

        for(int i = 0; i < steps.size(); i++) {
            ServerCommunication.getInstance().getDataTranslator().inputRecipeStep(Integer.toString(recipeID), steps.get(i));

        }
    }
}
