package no.ntnu.controllers;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseButton;
import javafx.stage.Stage;
import no.ntnu.FxApplication;
import no.ntnu.ServerDataTranslator;
import no.ntnu.entityclasses.ListHandler;
import no.ntnu.entityclasses.Product;
import no.ntnu.entityclasses.Recipe;
import no.ntnu.entityclasses.Step;
import no.ntnu.servermanagement.ServerCommunication;

/**
 * The controller for the RecipeView.fxml file.
 * It is responsible for displaying the recipe and handling the user's interaction with it.
 * 
 * @since 0.0.2
 * @version 0.0.3
 * @author Thomas Balsvik
 */
public class RecipeController implements javafx.fxml.Initializable {
    private RecipeListController recipeListController;
    @FXML
    private Label feedBackLabel;
    @FXML
    private ListView<Product> ingredientsListView;
    @FXML
    private ListView<String> stepsListView;
    @FXML
    private Label RecipeName;

    /**
     * Initializes the controller, while also setting up the ListView for 
     * ingredients and steps of a specific recipe. 
     * 
     * @since 0.0.2
     * @version 0.0.3
     * @author Joachim Duong
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        ListHandler.getInstance().setRecipeController(this);
        recipeListController = ListHandler.getInstance().getRecipeListController();
        RecipeName.setText(recipeListController.getSelectedRecipe().getName());
        updateUI();

        ingredientsListView.setCellFactory(param -> new ListCell<Product>() {
            @Override
            protected void updateItem(Product item, boolean empty) {
                super.updateItem(item, empty);
                if (empty || item == null) {
                    setText(null);
                } else {
                    setText(item.getName());
                }
            }
        });

        ingredientsListView.setOnMouseClicked(event -> {
            if (event.getButton() == MouseButton.PRIMARY && event.getClickCount() == 2) {
                // Get the selected item from the ListView
                Product product = ingredientsListView.getSelectionModel().getSelectedItem();
                ListHandler.getInstance().getShoppingController().addProduct(product);
                feedBackLabel.setText(String.format("%s added to shopping list", product.getName()));
            }
        });
    }

    /**
     * Updates the UI with the latest data from the server.
     *
     * @since 0.0.2
     * @version 0.0.2
     * @author Joachim Duong
     */
    public void updateUI() {
        ingredientsListView.getItems().clear();
        stepsListView.getItems().clear();
        recipeListController.getSelectedRecipe().clearProducts();
        recipeListController.getSelectedRecipe().clearSteps();
        updateFromServer();
        if (recipeListController.getSelectedRecipe() != null) {
            ingredientsListView.getItems().addAll(recipeListController.getSelectedRecipe().getProducts());
            ArrayList<Step> steps = recipeListController.getSelectedRecipe().getSteps();
            for(Step step : steps){
                stepsListView.getItems().add(step.getStep());
            }
        }
    }

    /**
     * Method for going back to the RecipeListView. 
     * @param actionEvent The automatically passed actionevent by clicking a button in JavaFX.
     * @throws IOException If the FXML file is not found. 
     * 
     * @since 0.0.2
     * @version 0.0.2
     * @author Thomas Balsvik
     */
    public void GoRecipeList(ActionEvent actionEvent)  throws IOException{
        Scene scene =((Node) actionEvent.getSource()).getScene();
        scene.setRoot(FxApplication.loadFXML("RecipeListView"));
    }

    /**
     * Method for going to the ShoppingListView. 
     * @param actionEvent The automatically passed actionevent by clicking a button in JavaFX.
     * @throws IOException If the FXML file is not found.
     */
    public void GoShoppingList(ActionEvent actionEvent) throws IOException {
        Scene scene =((Node) actionEvent.getSource()).getScene();
        scene.setRoot(FxApplication.loadFXML("ShoppingListView"));
    }

    /**
     * Method for going to the InventoryView. 
     * @param actionEvent The automatically passed actionevent by clicking a button in JavaFX.
     * @throws IOException If the FXML file is not found.
     */
    public void GoInventory(ActionEvent actionEvent) throws IOException {
        Scene scene =((Node) actionEvent.getSource()).getScene();
        scene.setRoot(FxApplication.loadFXML("InventoryView"));
    }

    /**
     * Method for opening a new window to edit the selected recipe. 
     * @param actionEvent The automatically passed actionevent by clicking a button in JavaFX.
     * @throws IOException
     */
    public void Edit(ActionEvent actionEvent) throws IOException {
        Parent root = loadFXML("RecipeAddView");
        // Create a new stage
        Stage secondStage = new Stage();
        secondStage.setTitle("Edit/Add Recipe");
        secondStage.setScene(new Scene(root, 800, 700));
        secondStage.show();    
    }

    /**
     * Opens a new window to add a product to the shopping list.
     * @param actionEvent The automatically passed actionevent by clicking a button in JavaFX.
     * @throws IOException If the FXML file is not found.
     * 
     * @since 0.0.2
     * @version 0.0.2
     * @author Thomas Balsvik
     */
    public void AddShopping(ActionEvent actionEvent) throws IOException {
        Parent root = loadFXML("AddShoppingCartView");
        Stage secondStage = new Stage();
        secondStage.setTitle("Add to shopping list");
        secondStage.setScene(new Scene(root, 1500, 900));
        secondStage.show();
    }

    /**
     * A method for loading FXML files.
     * @param fxmlFileName The filename without .fxml extension nor a beginning slash.
     * @return The parent node of the FXML file.
     * @throws IOException If the file is not found, meaning the fxmlFileName is incorrect.
     */
    public Parent loadFXML(String fxmlFileName) throws IOException {
        return FXMLLoader.load(FxApplication.class.getResource("/" + fxmlFileName + ".fxml"));
    }

    /**
     * Updates the recipe controller with the latest data from the server.
     * 
     * @since 0.0.2
     * @version 0.0.2
     * @author Joachim Duong
     */
    public void updateFromServer() {
        ingredientsListView.getItems().clear();
        stepsListView.getItems().clear();
        ServerDataTranslator translator = ServerCommunication.getInstance().getDataTranslator();

        ArrayList<String[]> ingredientsData = translator.getIngredientsData();
        ArrayList<String[]> stepData = translator.getStepsData();

        int i = ingredientsData.size();
        for(int n = 0; n < i; n++) {
            if(recipeListController.getSelectedRecipe().getRecipeId() == Integer.parseInt(ingredientsData.get(n)[1])){
                String[] ingredient = ingredientsData.get(n);
                Product currentProduct = new Product(ingredient[0],ingredient[3]);
                recipeListController.getSelectedRecipe().addProduct(currentProduct);
                System.out.println(currentProduct.getProductId());

            }
        }
        int j = stepData.size();
        for(int n = 0; n < j; n++) {
            if(recipeListController.getSelectedRecipe().getRecipeId() == Integer.parseInt(stepData.get(n)[1])){
                String[] step = stepData.get(n);
                recipeListController.getSelectedRecipe().addStep(step[2], Integer.parseInt(step[0]));
                System.out.println("First index" + step[0]);
                System.out.println("Second index" + step[1]);
                System.out.println("thrid index" + step[2]);
            }
        }
    }

}
