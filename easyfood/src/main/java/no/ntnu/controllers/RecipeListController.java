package no.ntnu.controllers;


import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseButton;
import javafx.stage.Stage;
import no.ntnu.FxApplication;
import no.ntnu.ServerDataTranslator;
import no.ntnu.entityclasses.*;
import no.ntnu.servermanagement.ServerCommunication;

/**
 * The controller for the RecipeList.fxml file.
 * It is responsible for displaying the recipe list and handling the user's interaction with it.
 * 
 * @since 0.0.2
 * @version 0.0.3
 * @author Nikolai Tønder
 */
public class RecipeListController implements javafx.fxml.Initializable{
    private RecipeManager recipeManager;
    @FXML
    private ListView<Recipe> recipeListView;
    private RecipeController recipeController;
    private Recipe selectedRecipe;

  @Override public void initialize(URL url, ResourceBundle rb) {
    // TODO
    recipeManager = ListHandler.getInstance().getRecipeManager();
    ListHandler.getInstance().setRecipeListController(this);
    updateUI();

    recipeListView.setCellFactory(param -> new ListCell<Recipe>() {
      @Override
      protected void updateItem(Recipe item, boolean empty) {
        super.updateItem(item, empty);
        if (empty || item == null) {
          setText(null);
        } else {
          // Display the result of toString() method of MyObject
          setText(item.toString());
        }
      }
    });

    recipeListView.setOnMouseClicked(event -> {
      if (event.getButton() == MouseButton.PRIMARY && event.getClickCount() == 2) {
        // Get the selected item from the ListView
        Recipe selectedItem = recipeListView.getSelectionModel().getSelectedItem();

        // Open the details window with the selected item
        openDetailsWindow(selectedItem);
      }
    });
  }

  public void updateUI() {
    updateFromServer();
    recipeListView.getItems().setAll(getRecipes());
  }

  private List<Recipe> getRecipes() {
    List<Recipe> recipes= new ArrayList<>();
    for (Recipe r : recipeManager.getRecipes().values()) {
      if(r != null){
        recipes.add(r);
      }
    }
    return recipes;
  }
  @FXML
  private Button loginButton;

  @FXML
  private void GoAddRecipe(ActionEvent event) throws IOException {
    Parent root = loadFXML("RecipeNameView");
    // Create a new stage
    Stage secondStage = new Stage();
    secondStage.setTitle("Name of the Recipe");
    //secondStage.setScene(new Scene(root, 850, 670));
    secondStage.setScene(new Scene(root, 300, 100));
    secondStage.show();
  }

  @FXML
  private void GoRecipe(ActionEvent event) throws IOException {

  }

  @FXML
  private void GoShoppingList(ActionEvent event) throws IOException {
    Scene test = ((Node) event.getSource()).getScene();
    test.setRoot(FxApplication.loadFXML("ShoppingListView"));
  }

  @FXML
  private void GoInventory(ActionEvent event) throws IOException {
    Scene test = ((Node) event.getSource()).getScene();
    test.setRoot(FxApplication.loadFXML("InventoryView"));
  }

  public Recipe getSelectedRecipe() {
    return selectedRecipe;
  }

  public void setSelectedRecipe(Recipe selectedRecipe) {
    this.selectedRecipe = selectedRecipe;
  }

  private void openDetailsWindow(Recipe selectedItem) {
    try {
      selectedRecipe = selectedItem;
      // Load the FXML file for the details window
      Scene test = recipeListView.getScene();
      test.setRoot(FxApplication.loadFXML("RecipeView"));

    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public void addRecipe(Recipe recipe) {
    recipeManager.addRecipe(recipe);
  }

  /**
   * Used to load FXML files. 
   * @param fxmlFileName the filename without .fxml extension nor a beginning slash
   * @return A parent object that can be used to display a new scene
   * @throws IOException if the file is not found, meaning the fxmlFileName is incorrect.
   * 
   * @since 0.0.1
   * @version 0.0.1
   * @author Rolf Normann Flatøy
   */
  public Parent loadFXML(String fxmlFileName) throws IOException {
    return FXMLLoader.load(FxApplication.class.getResource("/" + fxmlFileName + ".fxml"));
  }

  public void updateFromServer() {
    recipeManager.clear();
    ServerDataTranslator translator = ServerCommunication.getInstance().getDataTranslator();
    ArrayList<String> recipeList = translator.getRecipeList();
    int i = recipeList.size();
    for(int n = 0; n < i; n++) {
      int recipeID = Integer.parseInt(recipeList.get(n));
      Recipe currentRecipe = new Recipe(translator.getRecipeNameFromId(recipeID), getProductFromRecipe(recipeID), getStepForRecipe(recipeID));
      currentRecipe.setRecipeId(recipeID);
      recipeManager.addRecipe(currentRecipe);
    }
  }

  private ArrayList<Step> getStepForRecipe(int recipeID) {
    ArrayList<Step> steps = new ArrayList<>();
    ServerDataTranslator translator = ServerCommunication.getInstance().getDataTranslator();
    ArrayList<String[]> data = translator.getStepsData();
    for(int n=0; n<data.size(); n++){
      if(Integer.parseInt(data.get(n)[1]) == recipeID){
        Step s = new Step(data.get(n)[2], Integer.parseInt(data.get(n)[0]));
        steps.add(s);
      }
    }
    return steps;
  }

  private ArrayList<Product> getProductFromRecipe(int recipe) {
    ArrayList<Product> products = new ArrayList<>();
    ServerDataTranslator translator = ServerCommunication.getInstance().getDataTranslator();
    ArrayList<String[]> data = translator.getIngredientsData();
    for(int n=0; n<data.size(); n++){
      if(Integer.parseInt(data.get(n)[1]) == recipe){
        String ingredient = data.get(n)[0];
        int amount = Integer.parseInt(data.get(n)[2]);
        Product p = new Product(ingredient, amount);
        products.add(p);
      }
    }
    return products;
  }




}

