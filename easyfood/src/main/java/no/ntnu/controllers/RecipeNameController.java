package no.ntnu.controllers;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import no.ntnu.FxApplication;
import no.ntnu.entityclasses.*;
import no.ntnu.servermanagement.ServerCommunication;

/**
 * The controller for the RecipeNameView.fxml file.
 * It is responsible for adding a recipe to the recipe list.
 * 
 * @since 0.0.2
 * @version 0.0.3
 * @author Joachim Duong
 */
public class RecipeNameController implements javafx.fxml.Initializable {
    @FXML
    private TextField textField;
    private RecipeManager recipeManager;
    private RecipeController recipeController;
    private RecipeListController recipeListController;
    private Recipe selectedRecipe;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        recipeManager = ListHandler.getInstance().getRecipeManager();
        ListHandler.getInstance().setRecipeAddController(this);
        recipeListController = ListHandler.getInstance().getRecipeListController();
    }

    @FXML
    private void textFieldPressed(KeyEvent keyEvent) throws IOException {
        if (keyEvent.getCode().getName().equalsIgnoreCase("enter")) {
            try {
                Recipe newRecipe = new Recipe(textField.getText());
                recipeManager.addRecipe(newRecipe);
                recipeListController.setSelectedRecipe(newRecipe);
                addNewRecipe(newRecipe);
                goNextWindow();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void goNextWindow() throws IOException {
        Parent root = loadFXML("RecipeAddView");
        Stage stage = (Stage) textField.getScene().getWindow();
        stage.setHeight(700);
        stage.setWidth(900);
        stage.setScene(new Scene(root));

        stage.show();


    }

    public Parent loadFXML(String fxmlFileName) throws IOException {
        return FXMLLoader.load(FxApplication.class.getResource("/" + fxmlFileName + ".fxml"));
    }

    public void addNewRecipe(Recipe currentRecipe) {
        ServerCommunication.getInstance().getDataTranslator().inputRecipeData(currentRecipe.getName());
        int id = ServerCommunication.getInstance().getDataTranslator().getRecipeIdFromName(currentRecipe.getName());
        currentRecipe.setRecipeId(id);
        //addToServer(currentRecipe);
    }
}
