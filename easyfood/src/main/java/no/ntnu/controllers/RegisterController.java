package no.ntnu.controllers;

import java.io.IOException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import no.ntnu.FxApplication;
import no.ntnu.servermanagement.Command;
import no.ntnu.servermanagement.ServerCommunication;

/**
 * The controller for the RegisterView.fxml file.
 * It is responsible for registering new users.
 * 
 * @since 0.0.2
 * @version 0.0.3
 * @author Rolf Normann Flatøy
 */
public class RegisterController {

    @FXML
    private TextField uNameField;

    @FXML
    private PasswordField pwField;
    @FXML
    private PasswordField pwField2;

    @FXML
    private Button registerButton;

    char[] writableCharacters = {
        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
        'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
        'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
        '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '-', '_', '=', '+',
        '[', '{', ']', '}', '\\', '|', ';', ':', '\'', '"', ',', '<', '.', '>',
        '/', '?', '`'};

    /**
     * Registers a new user and logs in.
     * @param actionEvent the automatic event that is triggered when the button is clicked
     * @throws IOException if the FXML file is not found
     * 
     * @since 0.0.2
     */
    public void register(ActionEvent actionEvent) throws IOException {
        if(checkRepeatedPassword() && !checkEmptyFields()){
            if(registerUser(uNameField.getText(), pwField.getText())){
                Scene test =((Node) actionEvent.getSource()).getScene();
                test.setRoot(FxApplication.loadFXML("ShoppingListView"));
            } 
        }
    }

    private boolean registerUser(String username, String password) {
        Command command = ServerCommunication.getInstance().sendSpecifiedRequest(new String[] {username, password}, (short) 11);
        if(command.commandID == 12){
            System.out.println("User registe1red");
            return true;
        }
        else {
            System.out.println("User not regist2ered");
            return false;
        }
    }

    private boolean checkRepeatedPassword(){
        if(pwField.getText().equals(pwField2.getText())){
            return true;
        }
        else {
            return false;
        }
    }

    private boolean checkEmptyFields(){
        if(uNameField.getText().isEmpty() || pwField.getText().isEmpty() || pwField2.getText().isEmpty()){
            return true;
        }
        else {
            return false;
        }
    }

    @FXML
    public void keyTyped(KeyEvent keyEvent) {
        String text = pwField.getText();
        if (!keyEvent.getCharacter().matches("[0-9]") && isWritableChar(keyEvent)) {
            pwField.setText(pwField.getText().substring(0, pwField.getText().length() - 1));
        }
        else if(text.length() >= 5) {
            pwField.setText(text.substring(0, text.length() - 1));
        }
    }

    public boolean isWritableChar(KeyEvent c){
        for (char ch : writableCharacters) {
            if (c.getCharacter().charAt(0) == ch && c.getCharacter().length() == 1) {
                return true;
            }
        }
        return false;

    }
}
