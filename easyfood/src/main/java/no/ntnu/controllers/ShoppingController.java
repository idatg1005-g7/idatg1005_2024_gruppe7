package no.ntnu.controllers;

import java.io.IOException;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import no.ntnu.FxApplication;
import no.ntnu.ServerDataTranslator;
import no.ntnu.entityclasses.ListHandler;
import no.ntnu.entityclasses.Product;
import no.ntnu.entityclasses.ProductManager;
import no.ntnu.entityclasses.ShoppingList;
import no.ntnu.servermanagement.ServerCommunication;
// Parts of this class' javadoc is filled in by Copilot. 
/**
 * The controller for the ShoppingView.fxml file.
 * It is responsible for displaying the shopping list and handling the user's interaction with it. 
 * 
 * @since 0.0.2
 * @version 0.0.3
 * @author Joachim Duong
 */
public class ShoppingController implements javafx.fxml.Initializable{
    public Button GoInventory;
    public Button Remove;
    private ProductManager shopplingList;
    @FXML
    private ListView<String> ingredientList;

    @FXML
    private ListView<String> forList;
    @FXML
    private ListView<String> amountList;

    @FXML
    private ListView<String> costList;

    private int selectedIndex;

    /**
     * Initializes the controller, while also setting up the shopping list.
     * 
     * @since 0.0.2
     * @version 0.0.3
     * @author Joachim Duong
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        shopplingList = ListHandler.getInstance().getShoppingList();
        System.out.println("ShoppingController initialized");
        //setTestList();
        updateUI();
        synchList();
        setEventHandlers();
        ListHandler.getInstance().setShoppingController(this);

    }
    @FXML
    private void Add(ActionEvent event) throws IOException {
        Parent root = loadFXML("AddShoppingListView");
        // Create a new stage
        Stage secondStage = new Stage();
        secondStage.setTitle("Add Window");
        secondStage.setScene(new Scene(root, 900, 700));
        secondStage.show();
    }

    /**
     * Updates the UI with the current shopping list with the newest data from the server. 
     * 
     * @since 0.0.2
     */
    public void updateUI() {
        updateFromServer();

        ingredientList.getItems().setAll(getIngredients());
        forList.getItems().setAll(getFor());
        amountList.getItems().setAll(getAmount());
        costList.getItems().setAll(getCost());
    }

    public List<String> getIngredients() {
        List<String> ingredients = new ArrayList<>();
        for (Product p : shopplingList.getProducts()) {
            if(p.getName() != null){
                ingredients.add(p.getName());
            }
        }
        return ingredients;
    }

    private List<String> getFor() {
        List<String> forList = new ArrayList<>();
        for (Product p : shopplingList.getProducts()) {
            if(p.getCorrespondingRecipe() != null){
                forList.add(p.getCorrespondingRecipe());
            }
            else {
                forList.add("");
            }
        }
        return forList;
    }

    private List<String> getAmount() {
        List<String> amountList = new ArrayList<>();
        for (Product p : shopplingList.getProducts()) {
            amountList.add(Integer.toString(p.getQuantity()));
        }
        return amountList;
    }

    private List<String> getCost() {
        List<String> costList = new ArrayList<>();
        for (Product p : shopplingList.getProducts()) {
            if(p.getPrice() != 0){
                costList.add(Integer.toString(p.getPrice()));
            }
            else {
                costList.add("");
            }
        }
        return costList;
    }


    public void GoRecipes(ActionEvent actionEvent) throws IOException {
        Scene scene =((Node) actionEvent.getSource()).getScene();
        scene.setRoot(FxApplication.loadFXML("RecipeListView"));
    }

    public void GoShopping(ActionEvent actionEvent) {
    }

    public void GoInventory(ActionEvent actionEvent) throws IOException {
        Scene scene =((Node) actionEvent.getSource()).getScene();
        scene.setRoot(FxApplication.loadFXML("InventoryView"));
    }

    public void Remove(ActionEvent actionEvent) {
        removeProduct();
        updateUI();
    }

    private void synchList() {
        ingredientList.getSelectionModel().selectedIndexProperty().addListener((observable, oldValue, newValue) -> {
            synchronizeSelection(newValue.intValue());
        });

        forList.getSelectionModel().selectedIndexProperty().addListener((observable, oldValue, newValue) -> {
            synchronizeSelection(newValue.intValue());
        });

        amountList.getSelectionModel().selectedIndexProperty().addListener((observable, oldValue, newValue) -> {
            synchronizeSelection(newValue.intValue());
        });

        costList.getSelectionModel().selectedIndexProperty().addListener((observable, oldValue, newValue) -> {
            synchronizeSelection(newValue.intValue());
        });
    }

    private void synchronizeSelection(int selectedIndex) {
        if (selectedIndex >= 0) {
            ingredientList.getSelectionModel().select(selectedIndex);
            forList.getSelectionModel().select(selectedIndex);
            amountList.getSelectionModel().select(selectedIndex);
            costList.getSelectionModel().select(selectedIndex);
        }
    }

    private void setEventHandlers() {
        ingredientList.setOnMouseClicked(event -> handleListViewClick(ingredientList, event));
        forList.setOnMouseClicked(event -> handleListViewClick(forList, event));
        amountList.setOnMouseClicked(event -> handleListViewClick(amountList, event));
        costList.setOnMouseClicked(event -> handleListViewClick(costList, event));
    }

    private void handleListViewClick(ListView<String> listView, MouseEvent event) {
        selectedIndex = listView.getSelectionModel().getSelectedIndex();
    }

    public void addProduct(Product product) {
        shopplingList.addProduct(product);
    }

    private void removeProduct() {
        ServerCommunication.getInstance().getDataTranslator().deleteFromShoppingCart(Integer.toString(shopplingList.getProducts().get(selectedIndex).getProductId()));
        selectedIndex--;
        updateUI();
    }

    public Parent loadFXML(String fxmlFileName) throws IOException {
        return FXMLLoader.load(FxApplication.class.getResource("/" + fxmlFileName + ".fxml"));
    }

    private void updateFromServer(){
        shopplingList.clear();
        ServerDataTranslator translator = ServerCommunication.getInstance().getDataTranslator();
        int i = translator.getIngredientsShoppingList().size();
        for(int n = 0; n < i; n++){
            ArrayList<String[]> data = translator.getShoppingListData();
            String currentName = data.get(n)[0];
            int currentPrice = Integer.parseInt(data.get(n)[2]);
            int currentAmount = Integer.parseInt(data.get(n)[1]);
            String forString = data.get(n)[3];
            int itemId = Integer.parseInt(data.get(n)[4]);
            Product p = new Product(currentName, currentAmount, forString, currentPrice, itemId);
            shopplingList.addProduct(p);
        }
    }
}
