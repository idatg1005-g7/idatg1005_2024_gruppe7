package no.ntnu.entityclasses;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Class for managing the inventory of products
 * Extends from ProductManager to keep the list of products.
 * 
 * @author Rolf
 * @since 0.0.1
 * @version 0.0.1
 */
public class Inventory extends ProductManager{

    /**
     * Constructor for the Inventory class
     */
    public Inventory() {
        super();
    }


}
