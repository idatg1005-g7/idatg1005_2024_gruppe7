package no.ntnu.entityclasses;

import no.ntnu.controllers.InventoryController;
import no.ntnu.controllers.RecipeAddController;
import no.ntnu.controllers.RecipeController;
import no.ntnu.controllers.RecipeListController;
import no.ntnu.controllers.RecipeNameController;
import no.ntnu.controllers.ShoppingController;
// Parts of this class' javadoc is filled in by Copilot. 
/**
 * This is a class for handling all the list instances in the program.
 * This class is a singleton class, meaning that there can only be one instance of this class.
 *
 * @author Joachim Duong
 * @since 0.0.2
 * @version 0.0.2
 */
public class ListHandler {
    private ProductManager shoppingList;
    private ShoppingController shoppingController;
    private InventoryController inventoryController;
    private RecipeListController recipeListController;
    private RecipeController recipeController;
    private RecipeNameController recipeNameController;
    private RecipeAddController recipeAddController;
    private ProductManager inventory;
    private RecipeManager recipeManager;
    private static ListHandler instance;

    private ListHandler() {
        shoppingList = new ShoppingList();
        inventory = new Inventory();
        recipeManager = new RecipeManager();
    }

    /**
     * Returns the instance of the ListHandler class.
     * This instance is always connected to the server when it is returned.
     *
     * @return The instance of the ListHandler class.
     * @since 0.0.2
     */
    public static ListHandler getInstance() {
        if (instance == null) {
            instance = new ListHandler();
        }
        return instance;
    }

    /**
     * @return The shopping list
     * @since 0.0.2
     */
    public ProductManager getShoppingList() {
        return shoppingList;
    }

    /**
     * @return The inventory
     * @since 0.0.2
     */
    public ProductManager getInventory() {
        return inventory;
    }

    /**
     * @return the recipe manager
     * @since 0.0.2
     */
    public RecipeManager getRecipeManager() {
        return recipeManager;
    }

    /**
     * Sets the shopping controller
     * @param shoppingController The shopping controller to set
     * @since 0.0.2
     */
    public void setShoppingController(ShoppingController shoppingController) {
        this.shoppingController = shoppingController;
    }

    /**
     * @return The shopping controller
     * @since 0.0.2
     */
    public ShoppingController getShoppingController() {
        return shoppingController;
    }

    /**
     * Sets the inventory controller
     * @param inventoryController The inventory controller to set
     * @since 0.0.2
     */
    public void setInventoryController(InventoryController inventoryController) {
        this.inventoryController = inventoryController;
    }

    /**
     * @return The inventory controller
     * @since 0.0.2
     */
    public InventoryController getInventoryController() {
        return inventoryController;
    }

    /**
     * Sets the recipe list controller
     * @param recipeController The recipe list controller to set
     * @since 0.0.2
     */
    public void setRecipeListController(RecipeListController recipeController) {
        this.recipeListController = recipeController;
    }

    /**
     * @return The recipe list controller
     * @since 0.0.2
     */
    public RecipeListController getRecipeListController() {
        return recipeListController;
    }

    /**
     *
     * @param recipeController The recipe controller to set
     * @since 0.0.2
     */

    public void setRecipeController(RecipeController recipeController) {
        this.recipeController = recipeController;
    }

    /**
     *
     * @return The recipe controller
     * @since 0.0.2
     */
    public RecipeController getRecipeController() {
        return recipeController;
    }

    /**
     * Sets the recipe name controller
     * @param recipeNameController The recipe name controller to set
     * @since 0.0.2
     */
    public void setRecipeAddController(RecipeNameController recipeNameController) {
        this.recipeNameController = recipeNameController;
    }

    /**
     * Gets the recipe name controller.
     * @return The recipe name controller.
     * 
     * @since 0.0.1
     */
    public RecipeAddController getRecipeAddController() {
        return recipeAddController;
    }

    /**
     * Sets the recipe add controller.
     * @param recipeAddController
     * 
     * @since 0.0.1
     */
    public void setRecipeAddController(RecipeAddController recipeAddController) {
        this.recipeAddController = recipeAddController;
    }

}
