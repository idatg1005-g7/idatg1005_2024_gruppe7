package no.ntnu.entityclasses;

// Parts of this class' javadoc is filled in by Copilot. 
/**
 * Product Entity Class, used to represent a product in the system.
 * A product is something that can be bought and used in a recipe.
 * It has a name, price, corresponding recipe and quantity.
 * The quantity is used to represent how many of the product is in the inventory.
 * 
 * @author Joachim Duong
 * @since 0.0.1
 * @version 0.0.1
 */
public class Product {
    private String name;
    private int price;
    private String correspondingRecipe;
    private int quantity;
    private String recipe;
    private int productId;

    private String expirationDate;

    /**
     * Constructor for the Product class
     * @param name the name of the product
     * @param quantity the quantity of the product
     * @param correspondingRecipe the recipe that uses this product
     * @param price the price of the product
     * @param itemId the id of the product
     * 
     * @since 0.0.1
     */
    public Product(String name, int quantity, String correspondingRecipe, int price, int itemId) {
        setName(name);
        setPrice(price);
        setCorrespondingRecipe(correspondingRecipe);
        setQuantity(quantity);
        setProductId(itemId);
    }

    /**
     * Constructor for the Product class
     * @param name the name of the product
     * 
     * @since 0.0.1
     */
    public Product(String name) {
        setName(name);
    }

    /**
     * Constructor for the Product class
     * @param name the name of the product
     * @param itemId the id of the product
     * 
     * @since 0.0.1
     */
    public Product(String name, String itemId) {
        setName(name);
        setProductId(Integer.parseInt(itemId));
    }

    /**
     * Constructor for the Product class
     * @param name the name of the product
     * @param quantity the quantity of the product
     * 
     * @since 0.0.1
     */
    public Product(String name, int quantity) {
        setName(name);
        setQuantity(quantity);
    }

    /**
     * Constructor for the Product class
     * @param name the name of the product
     * @param expirationDate the expiration date of the product
     * @param quantity the quantity of the product
     * @param itemId the id of the product
     * 
     * @since 0.0.1
     */
    public Product(String name, String expirationDate, int quantity, int itemId) {
        setName(name);
        setExpirationDate(expirationDate);
        setQuantity(quantity);
        setProductId(itemId);
    }

    /**
     * Constructor for the Product class
     * @param name the name of the product
     * @param quantity the quantity of the product
     * @param correspondingRecipe the recipe that uses this product
     * 
     * @since 0.0.1
     */
    public Product(String name, int quantity, String correspondingRecipe) {
        setName(name);
        setPrice(quantity);
        setCorrespondingRecipe(correspondingRecipe);
    }

    /**
     * Constructor for the Product class
     * @param name the name of the product
     * @param quantity the quantity of the product
     * @param price the price of the product
     * 
     * @since 0.0.1
     */
    public Product(String name, int quantity, int price) {
        setName(name);
        setPrice(price);
        setCorrespondingRecipe(correspondingRecipe);
        setQuantity(quantity);
    }

    /**
     * Get the quantity of the product.
     * @return the quantity of the product
     * 
     * @since 0.0.1
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * Set the quantity of the product. If the quantity is negative, an exception is thrown.
     * @param quantity the quantity of the product
     * @throws IllegalArgumentException if the quantity is negative
     * 
     * @since 0.0.1
     */
    public void setQuantity(int quantity) {
        if (quantity < 0) 
            throw new IllegalArgumentException("Quantity cannot be negative.");

        this.quantity = quantity;
    }
    
    /**
     * Gets the name of the product.
     * 
     * @since 0.0.1
     */
    public String getName()
    {
        return name;
    }

    /**
    * Set the name of the product.
    * @param name the name of the product
    * 
    * @since 0.0.1
    */
    public void setName(String name) {
        if (name == null || name.isEmpty()){
            throw new IllegalArgumentException("Name cannot be empty.");
        }
        this.name = name;
    }

    /**
     * Get the price of the product.
     * @return the price of the product
     * 
     * @since 0.0.1
     */
    public int getPrice() {
        return price;
    }

    /**
     * Set the price of the product. If the price is negative, an exception is thrown.
     * @param price the price of the product
     * @throws IllegalArgumentException if the price is negative
     * 
     * @since 0.0.1
     */
    public void setPrice(int price) {
        if (price < 0) {
            throw new IllegalArgumentException("Price cannot be negative.");
        }
        this.price = price;
    }

    /**
     * Get the recipe that uses this product.
     * @return the recipe that uses this product
     * 
     * @since 0.0.1
     */
    public String getCorrespondingRecipe() {
        return correspondingRecipe;
    }
    
    /**
     * Set the recipe that uses this product.
     * @param correspondingRecipe the recipe that uses this product
     * 
     * @since 0.0.2
     */
    public void setCorrespondingRecipe(String correspondingRecipe) {
        this.correspondingRecipe = correspondingRecipe;
    }

    /**
     * @return the recipe that uses this product.
     * 
     * @since 0.0.1
     */
    public String getRecipe() {
        return recipe;
    }

    /**
     * Set the recipe that uses this product.
     * @param recipe the recipe that uses this product
     * 
     * @since 0.0.1
     */
    public void setRecipe(String recipe) {
        this.recipe = recipe;
    }

    /**
     * Gets the expiration date.
     * @return the expiration date.
     * 
     * @since 0.0.1
     */
    public String getExpirationDate() {
        return expirationDate;
    }

    /**
     * Sets the expiration date.
     * @param expirationDate the expiration date.
     * 
     * @since 0.0.1
     */
    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    /**
     * Sets the product id.
     * @param productId the product id.
     * 
     * @since 0.0.1
     */
    public void setProductId(int productId) {
        this.productId = productId;
    }

    /**
     * Gets the product id.
     * @return the product id.
     * 
     * @since 0.0.1
     */
    public int getProductId() {
        return productId;
    }
}
