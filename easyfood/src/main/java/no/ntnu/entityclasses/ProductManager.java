package no.ntnu.entityclasses;

import java.util.ArrayList;
import java.util.List;

import no.ntnu.servermanagement.Command;
import no.ntnu.servermanagement.ServerCommunication;

/**
 * Class Where the products list is contained.
 * 
 * @author Rolf Normann Flatøy
 * @since 0.0.1
 * @version 0.0.1
 */
public class ProductManager {

    private final ArrayList<Product> products;

    /**
     * Constructor for the ProductManager
     * 
     * @since 0.0.1
     */
    public ProductManager() {
        products = new ArrayList<>();
    }

    /**
     * Adds a product to the list
     * 
     * @since 0.0.1
     */
    public void addProduct(Product p) {
        products.add(p);
    }

    /**
    * @return the list of products
    * 
    * @since 0.0.1
    */
    public List<Product> getProducts() {
        return products;
    }

  /**
   * Removes a product from the list by index
   * @param index
   * @return true if the product was removed, false if not
   * 
   * @since 0.0.1
   */
  public boolean removeByIndex(int index) {
      if (products != null && index >= 0 && index < products.size()) {
            products.remove(index);
            return true;
      }
      return false;
  }

    /**
     * Clears the list of products
     */
    public void clear() {
        products.clear();
    }
}
