package no.ntnu.entityclasses;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Class for representing a recipe.
 * A recipe is a list of products that are used to make a dish.
 * The recipe has a name and a list of products.
 * 
 * @author Joachim
 * @version 0.0.1
 * @since 0.0.1
 */
public class Recipe {
    private ArrayList<Product> products;
    private ArrayList<Step> steps;
    private String name;
    private int recipeId;

    /**
     * Constructor for the Recipe class
     * @param name the name of the recipe
     * 
     */
    public Recipe(String name, ArrayList<Product> products, ArrayList<Step> steps) {
        this.name = name;
        this.products = products;
        this.steps = steps;
    }

    /**
     * Constructor for the Recipe class
     * @param name the name of the recipe
     * 
     * @since 0.0.1
     */
    public Recipe(String name) {
        this.name = name;
        products = new ArrayList<>();
        steps = new ArrayList<>();
    }

    public ArrayList<Product> getProducts() {
        return products;
    }

    /**
     * Gets the products in the recipe as a list of strings
     * @return ArrayList of products as strings
     * 
     * @since 0.0.1
     */
    public ArrayList<String> getProductsAsString() {
        ArrayList<String> productsAsString = new ArrayList<>();
        for (Product p : products) {
            productsAsString.add(p.getName());
        }
        return productsAsString;
    }

    /**
     * Adds a product to the recipe
     * @param p the product to be added
     * 
     * @since 0.0.1
     */
    public void addProduct(Product p) {
        products.add(p);
    }

    /**
     * Gets the name of the Recipe.
     * @return name of the recipe
     * 
     * @since 0.0.1
     */
    public String getName() {
        return name;
    }


    /**
     * Retrieves the steps of the recipe as a list
     * @return recipe steps
     * 
     * @since 0.0.1
     */
    public ArrayList<Step> getSteps() {
        return steps;
    }

    /**
     * Retrieves the steps of the recipe as a list of strings
     * @return ArrayList of steps as strings
     * 
     * @since 0.0.2
     */
    public ArrayList<String> getStepsAsString() {
        ArrayList<String> stepsAsString = new ArrayList<>();
        for (Step s : steps) {
            stepsAsString.add(s.getStep());
        }
        return stepsAsString;
    }

    /**
     * Adds a step to the recipe
     * @param step the step to be added
     * 
     * @since 0.0.1
     */
    public void addStep(String step, int stepId) {
        steps.add(new Step(step, stepId));
    }

    /**
     * Makes a string representation of the recipe name
     * @return the name of the recipe
     * 
     * @since 0.0.1
     */
    @Override
    public String toString() {
        return name;
    }

    /**
     * Moves the recipe step up in the list
     * @param index
     * 
     * @since 0.0.1
     */
    public void moveStepUp(int index){
        Collections.swap(steps, index, index - 1);
    }

    /**
     * Moves the recipe step down in the list
     * @param index
     * 
     * @since 0.0.2
     */
    public void moveStepDown(int index){
        Collections.swap(steps, index, index + 1);
    }

    /**
     * Moves the recipe product up in the list
     * @param index
     * 
     * @since 0.0.2
     */
    public void moveProductUp(int index){
        Collections.swap(products, index, index - 1);
    }

    /**
     * Moves the recipe product down in the list
     * @param index
     * 
     * @since 0.0.2
     */
    public void moveProductDown(int index){
        Collections.swap(products, index, index + 1);
    }

    /**
     * Sets the id of the recipe
     * @param recipeId
     * 
     * @since 0.0.1
     */
    public void setRecipeId(int recipeId) {
        this.recipeId = recipeId;
    }

    /**
     * Gets the id of the recipe
     * @return the id of the recipe
     * 
     * @since 0.0.1
     */
    public int getRecipeId() {
        return recipeId;
    }

    /**
     * Removes a step from the recipe
     * @param stepId
     * 
     * @since 0.0.1
     */
    public void removeStep(int stepId) {
        for (int i = 0; i < steps.size(); i++) {
            if (steps.get(i).getStepId() == stepId) {
                steps.remove(i);
            }
        }
    }

    /**
     * Clears the products from the recipe
     * 
     * @since 0.0.1
     */
    public void clearProducts() {
        products.clear();
    }

    /**
     * Clears the steps from the recipe
     * 
     * @since 0.0.1
     */
    public void clearSteps() {
        steps.clear();
    }
}

