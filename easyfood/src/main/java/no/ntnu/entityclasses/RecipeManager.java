package no.ntnu.entityclasses;
import java.util.HashMap;

/**
 * Class for managing the recipes
 * The recipe manager contains a hashmap of recipes
 * 
 * @since 0.0.2
 * @version 0.0.3
 * @author Joachim Duong
 */
public class RecipeManager {
    private HashMap<String, Recipe> recipes;

    /**
    * Constructor for RecipeManager
    * 
    * @since 0.0.2
    */
    public RecipeManager() {
        this.recipes = new HashMap<>();
    }

    /**
    * Adds a recipe to the recipe manager
    * @param recipe the recipe to be added
    * 
    * @since 0.0.2
    */
    public void addRecipe(Recipe recipe) {
        this.recipes.put(recipe.getName(), recipe);
    }

    /**
    * Gets a recipe from the recipe manager
    * @param name the name of the recipe to be retrieved
    * @return the recipe with the given name
    * 
    * @since 0.0.2
    */
    public Recipe getRecipe(String name) {
        return this.recipes.get(name);
    }

    /**
    * Removes a recipe from the recipe manager
    * @param name the name of the recipe to be removed
    * 
    * @since 0.0.2
    */
    public void removeRecipe(String name) {
        this.recipes.remove(name);
    }

    /**
    * Gets all recipes from the recipe manager
    * @return a hashmap containing all recipes
    * 
    * @since 0.0.2
    */
    public HashMap<String, Recipe> getRecipes() {
        return this.recipes;
    }

    /** 
     * Clears the recipe manager
     * 
     * @since 0.0.2
     */
    public void clear(){
        recipes.clear();
    }

    /**
     * Gets a recipe from the recipe manager by id
     * @param id the id of the recipe to be retrieved
     * @return the recipe with the given id
     * 
     * @since 0.0.3
     */
    public Recipe getRecipeFromId(int id) {
        for (Recipe r : recipes.values()) {
            if (r.getRecipeId() == id) {
                return r;
            }
        }
        return null;
    }


}
