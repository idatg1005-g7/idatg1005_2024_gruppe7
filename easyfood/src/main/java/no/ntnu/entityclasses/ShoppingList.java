package no.ntnu.entityclasses;

/**
 * Class for managing the shopping list of products
 * Extends from ProductManager to keep the list of products.
 * 
 * @since 0.0.2
 * @version 0.0.3
 * @author Joachim Duong
 */
public class ShoppingList extends ProductManager{

    /**
     * Constructor for the ShoppingList class
     * 
     * @since 0.0.2
     */
    public ShoppingList() {
        super();
    }

}
