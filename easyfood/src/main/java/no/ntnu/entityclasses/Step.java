package no.ntnu.entityclasses;

/**
 * Step Entity Class, used to represent a step in a recipe.
 * 
 * @since 0.0.2
 * @version 0.0.3
 * @author Joachim Duong
 */
public class Step {
    private String step;
    private int stepId;

    /**
     * Constructor for the Step class, requiring both variables to be set.
     * @param step the step in the recipe
     * @param stepId the id of the step corresponding to the primary key in the database
     * 
     * @since 0.0.2
     */
    public Step(String step, int stepId) {
        this.step = step;
        this.stepId = stepId;
    }

    /**
     * @return the step description for this step
     */
    public String getStep() {
        return step;
    }

    /**
     * @return the step id for this step. It corresponds to the primary key in the database.
     */
    public int getStepId() {
        return stepId;
    }

    /**
     * @param step the step description to set
     */
    public void setStep(String step) {
        this.step = step;
    }

    /**
     * @param stepId the step id to set. It should correspond to the primary key in the database.
     */
    public void setStepId(int stepId) {
        this.stepId = stepId;
    }
}
