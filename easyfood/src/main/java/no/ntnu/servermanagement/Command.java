package no.ntnu.servermanagement;

import java.util.ArrayList;

/**
 * This class the main method of communication between the client and the server.
 * The commands are sent and received through this class.
 * 
 * @since 0.0.2
 * @version 0.0.3
 * @author Rolf Normann Flatøy
 */
public class Command {
    public final short commandID;
    public final String[] commandContent;
    public final byte[] bytePayload;
    public final ArrayList<String[]> databaseResponse;

    
    /**
     * Constructor for the Command class when sending a specified request
     * @param commandNumber the command number to send to the server
     * @param commandContent the strings to inform the server of the request. 
     * 
     * @since 0.0.2
     */
    public Command(short commandNumber, String[] commandContent) {
        this.commandID = commandNumber;
        this.commandContent = commandContent;
        this.bytePayload = null;
        this.databaseResponse = null;
    }

    /**
     * Constructor for the Command class when sending a specified request
     * @param commandNumber the command number to received from the server
     * @param payload the byte array received from the server
     * 
     * @since 0.0.2
     */
    public Command(short commandNumber, byte[] payload) {
        this.bytePayload = payload;
        this.commandID = commandNumber;
        this.commandContent = null;
        this.databaseResponse = null;
    }

    /**
     * Constructor for the Command class when receiving data from the database
     * @param commandNumer the command number received from the server
     * @param databaseResponse the database response received from the server
     * 
     * @since 0.0.2
     */
    public Command(short commandNumer, ArrayList<String[]> databaseResponse) {
        this.databaseResponse = databaseResponse;
        this.commandID = commandNumer;
        this.bytePayload = null;
        this.commandContent = null;
    }
}
