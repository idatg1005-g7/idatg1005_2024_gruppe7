package no.ntnu.servermanagement;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Properties;

import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

import no.ntnu.ServerDataTranslator;

/**
 * This class is responsible for the communication between the client and the server.
 * It sends requests to the server and receives responses from the server.
 * 
 * @since 0.0.1
 * @version 0.0.3
 * @author Rolf Normann Flatøy
 */
public class ServerCommunication {
    
    private SSLSocket socket;
    private static String charset = "UTF-8";
    private static ServerCommunication instance;
    private ServerDataTranslator dataTranslator = new ServerDataTranslator();

    private ServerCommunication() {

    }

    /**
     * Returns the instance of the ServerCommunication class. 
     * This instance is always connected to the server when it is returned.
     * 
     * @return The instance of the ServerCommunication class.
     * 
     * @since 0.0.2
     */
    public static ServerCommunication getInstance() {
        if (instance == null) {
            instance = new ServerCommunication();
            instance.connectToServer();
        }
        return instance;
    }

    /**
     * Sends a request to the server. The server will return a string with 
     * the expected format depending on the request. 
     * 
     * @since 0.0.1
     */
    public Command sendRequest(byte[] message, short commandID) {
        try {
            socket.getOutputStream().write(constructMessage(message, commandID).array());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return getResponse(socket, commandID);
    }

    /**
     * Constructs the message as the server expect to receive multiple strings. 
     * This method should be used to tell the server of multiple variables; eg. 
     * username and password.
     * 
     * See the ServerInputOutput.txt in the resources folder. 
     * 
     * @param contents The strings to send to the server. See wiki for documentation
     * @return The response string from the server.
     * 
     * @since 0.0.1
     */
    public Command sendSpecifiedRequest(String[] contents, short commandID) {
        byte[] request = concatenateElementsToRequest(contents);
        return sendRequest(request, commandID);
    }

    private byte[] concatenateElementsToRequest(String[] contents) {
        int length = 0;
        for (int i = 0; i < contents.length; i++) {
            try {
                length += 4 + contents[i].getBytes(charset).length;
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        ByteBuffer bytes = ByteBuffer.allocate(length);
        for (int i = 0; i < contents.length; i++) {
            bytes.putInt(contents[i].getBytes().length);
            bytes.put(contents[i].getBytes(), 0, contents[i].getBytes().length);
        }
        return bytes.array();
    }

    private ByteBuffer constructMessage(byte[] message, short commandID) {
        int messageLength = message.length;
        byte[] bytes = new byte[messageLength + 4 + 2];
        ByteBuffer buffer = ByteBuffer.wrap(bytes).order(ByteOrder.BIG_ENDIAN);
        buffer.putInt(messageLength);
        buffer.putShort(commandID);
        buffer.put(message);
        return buffer;
    }

    /**
     * Establishes a connection to the server. This method needs to be called 
     * before calling methods like sendRequest().
     */
    private void connectToServer() {
        Properties connectionProperties = loadProperties();
        String serverAddress = connectionProperties.getProperty("serverAddress");
        int serverPort = Integer.parseInt(connectionProperties.getProperty("serverPort"));
        socket = createSocket(serverAddress, serverPort);
    }

    private SSLSocket createSocket(String ip, int port) {
        SSLSocketFactory sslSocketFactory = (SSLSocketFactory) SSLSocketFactory.getDefault();
        SSLSocket socket = null;
        try {
            socket = (SSLSocket) sslSocketFactory.createSocket(ip, port);
        } catch (UnknownHostException e) {
            System.err.println("Could not find the host, check internet connection.");
            System.exit(1);
        } catch (IOException e) {
            System.err.println("Could not establish a connection to the host. IOException: ");
            e.printStackTrace();
            System.exit(1);
        }
        return socket;
    }

    private Properties loadProperties() {
        Properties properties = new Properties();
        try (InputStream s = this.getClass().getResourceAsStream("/connections.properties")) {
            if (s == null) {
                System.out.println(System.getProperty("java.class.path"));
                // TODO get a logger
                System.err.println("Couldn't find properties file");
                System.exit(1);
            }
            properties.load(s);
        } catch (IOException ex) {
            // TODO get a logger
            System.err.println(ex.getStackTrace());
            System.exit(1);
        }
        return properties;
    }

    private Command getResponse(Socket socket, short commandID) {
        byte[] packageLength = readNBytes(socket, 4);
        if (packageLength == null) {
            // TODO re-establish server connection
            return new Command((short)-1, new String[] {});
        }
        ByteBuffer buffer = ByteBuffer.wrap(packageLength,0,4).order(ByteOrder.BIG_ENDIAN);
        int toread = buffer.getInt();
        return readNBytesToString(socket, toread, commandID);
    }

    private Command readNBytesToString(Socket socket, int nBytes, short commandID) {
        byte[] response = readNBytes(socket, nBytes);

        if (commandID == (short)11) {
            return new Command(ByteBuffer.wrap(response).getShort(), new String[] {});
        }
        if (commandID == (short)10) {
            return new Command(ByteBuffer.wrap(response).getShort(), new String[] {});
        }
        return extractContentFromServer(response, false);
    }

    private String[] getStringFromBytes(ByteBuffer buffer) {
        ArrayList<String> strings = new ArrayList<>();
        while (buffer.remaining() > 0) {
            int length = buffer.getInt();
            byte[] toReturn = new byte[length];
            buffer.get(buffer.position(), toReturn, 0, length);
            buffer.position(buffer.position() + length);
            try {
                strings.add(new String(toReturn, charset));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        return strings.toArray(new String[0]);
    }

    private Command extractContentFromServer(byte[] bytes, boolean ignoreShort) {
        ArrayList<String[]> strings = new ArrayList<String[]>();
        ByteBuffer buffer = ByteBuffer.wrap(bytes);
        short commandNumber = -1;
        if (!ignoreShort)
            commandNumber = buffer.getShort();

        while (0 < buffer.remaining()) {
            int stringLength = buffer.getInt();
            int consumed = 0;
            while (consumed < stringLength) {
                int elementLength = buffer.getInt();
                consumed += 4;
                byte[] currentString = new byte[elementLength];
                buffer.get(currentString, 0, elementLength);
                consumed += elementLength;
                strings.add(getStringFromBytes(ByteBuffer.wrap(currentString)));
            }
        }
        return new Command(commandNumber, strings);
    }
    
    private byte[] readNBytes(Socket socket, int nBytes) {
        InputStream stream = null;
        try {
            stream = socket.getInputStream();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        int bytesRead = 0;
        byte[] bytes = new byte[nBytes];
        while (bytesRead < nBytes) {
            try {
                int read = stream.read(bytes, bytesRead, nBytes - bytesRead);
                if (read == -1) {
                    System.err.println("Reached end of stream when not supposed to, " +
                    "unexpected client behavior");
                    return null;
                }
                bytesRead += read;
            } catch (IOException e) {
                System.err.println("Failed reading from client socket: ");
                e.printStackTrace();
                return null;
            }
        }
        return bytes;
    }

    /**
     * @return the instance of ServerDataTranslator.
     */
    public ServerDataTranslator getDataTranslator() {
        return dataTranslator;
    }
}
