package no.ntnu.entityclasses;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ProductManagerTest {

  @BeforeEach
  void setUp() {
  }

  @AfterEach
  void tearDown() {
  }

  // @Test
  // void addProduct() {
  //   ProductManager pm = new ProductManager();
  //   Product p = new Product();
  //   pm.addProduct(p);
  //   assertEquals(p, pm.findProduct());
  // }

  // @Test
  // void findProductPositive() {
  //   ProductManager pm = new ProductManager();
  //   Product p = new Product(1);
  //   pm.addProduct(p);
  //   assertEquals(p, pm.findProduct(1));
  // }

  // @Test
  // void findProductNegative() {
  //   ProductManager pm = new ProductManager();
  //   Product p = new Product(1);
  //   pm.addProduct(p);
  //   assertNull(pm.findProduct(2));
  // }

  // @Test
  // void removeProductPositive() {
  //   ProductManager pm = new ProductManager();
  //   Product p = new Product(1);
  //   pm.addProduct(p);
  //   assertTrue(pm.removeProduct(p));
  // }

  // @Test
  // void removeProductNegative1() {
  //   ProductManager pm = new ProductManager();
  //   assertFalse(pm.removeProduct(null));
  // }
  // @Test
  // void removeProductNegative2() {
  //   ProductManager pm = new ProductManager();
  //   Product p = new Product(1);
  //   pm.addProduct(p);
  //   assertFalse(pm.removeProduct(new Product(2)));
  // }
}