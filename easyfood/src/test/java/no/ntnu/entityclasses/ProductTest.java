package no.ntnu.entityclasses;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class ProductTest {

  @org.junit.jupiter.api.BeforeEach
  void setUp() {
  }

  @org.junit.jupiter.api.AfterEach
  void tearDown() {
  }

   @Test
   void getID() {
     Product p = new Product("Test", "1");
     assertEquals(1, p.getProductId());
   }

}