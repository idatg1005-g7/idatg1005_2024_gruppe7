package no.ntnu.entityclasses;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import no.ntnu.entityclasses.Recipe;
import no.ntnu.entityclasses.RecipeManager;

class RecipeManagerTest {

  @BeforeEach
  void setUp() {
  }

  @AfterEach
  void tearDown() {
  }

  @Test
  void addRecipe() {
    RecipeManager rm = new RecipeManager();
    Recipe r = new Recipe("Test");
    rm.addRecipe(r);
    assertEquals(r, rm.getRecipe("Test"));
  }

  @Test
  void removeRecipe() {
    RecipeManager rm = new RecipeManager();
    Recipe r = new Recipe("Test");
    rm.addRecipe(r);
    rm.removeRecipe("Test");
    assertNull(rm.getRecipe("Test"));
  }

  @Test
  void getRecipes() {
    RecipeManager rm = new RecipeManager();
    Recipe r = new Recipe("Test");
    rm.addRecipe(r);
    assertEquals(r, rm.getRecipes().get("Test"));
  }
}