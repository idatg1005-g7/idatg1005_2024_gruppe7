package no.ntnu.entityclasses;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class RecipeTest {

  @BeforeEach
  void setUp() {
  }

  @AfterEach
  void tearDown() {
  }

   @Test
   void getProductsPositive() {
     Recipe r = new Recipe("Test");
     Product p = new Product("Cheese");
     r.addProduct(p);
     assertEquals(p, r.getProducts().get(0));
   }

   @Test
   void getProductsNegative() {
     Recipe r = new Recipe("Test");
     Product p = new Product("Cheese");
     r.addProduct(p);
     assertNotEquals(new Product("bread"), r.getProducts().get(0));
   }

   @Test
   void getName() {
     Recipe r = new Recipe("Test");
     assertEquals("Test", r.getName());
   }
}